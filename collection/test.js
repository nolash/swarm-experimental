let collection = require('./collection');
let swarm = require('swarm-lowlevel');

// hash the actual content
//let hasher = new swarm.fileHasher(console.debug);
let hasher = new swarm.fileHasher();
let buf = Buffer.from("foo");
let refOne = hasher.Hash(buf);

hasher.Reset();
buf = Buffer.from("bar");
let refTwo = hasher.Hash(buf);

hasher.Reset();
buf = Buffer.from("baz");
let refThree = hasher.Hash(buf);

// create a collection for the content
let c = new collection.Collection();
let cidx = c.add(refOne);
if (cidx != 0) {
	console.error("idx mismatch", cidx);
	process.exit(1);	
}
cidx = c.add(refTwo);
if (cidx != 1) {
	console.error("idx mismatch", cidx);
	process.exit(1);	
}
c.add(refThree);
c_serialized = c.serialize();
console.debug('collection serialized', c_serialized);

let c2 = new collection.Collection();
if (!c2.deserialize(c_serialized)) {
	console.err("oops");
}

console.debug('c2', c2);
for (let i = 0; i < c.hashes; i++) {
	for (let j = 0; j < 32; j++) {
		if (c.hashes[i][j] != c2.hashes[i][j]) {
			console.error('collection mismatch', i, j, c.hashes[i][j], c2.hashes[i][j]);
		}
	}
}
//console.debug(c2);
//c2.each(console.debug);


// create the manifest index
let idx = new collection.CollectionIndex("manifest");
idx.add({
	filename: "foo.txt",
	mimetype: "text/plain",	
}, 0);
idx.add({
	filename: "media/bar.png",
	mimetype: "image/png",	
}, 1);
idx.add({
	filename: "media/plugh.png",
	mimetype: "image/png",	
}, 1);
idx.add({
	filename: "meditation/baz.txt",
	mimetype: "text/plain",
}, 0);
idx.add({
	filename: "xyzzy",
	mimetype: "application/octet-stream",
}, 2);
idx.add({
	filename: "txyzsdacsafzy",
	mimetype: "application/octet-stream",
}, 1);
console.debug('index', idx);
let idx_serialized = idx.serialize();
console.debug('index serialized', idx_serialized);


let idx2 = new collection.CollectionIndex("manifest");
if (!idx2.deserialize(idx_serialized)) {
	console.debug('index deserialized', idx2);
}

for (const e in idx.keys) {
	if (idx.values[idx.keys[e]] != idx2.values[idx2.keys[e]]) {
		console.error('idx mismatch', e, idx.values[idx.keys[e]] != idx2.values[idx2.keys[e]]);
	}
}


// wrap with the swarm adapter and check values are correct
let a = new collection.SimpleManifestAdapter(c, idx);

for (const k in idx.keys) {
	let value_idx = idx.keys[k];
	let collection_idx = idx.values[value_idx];
	let gottenRef = a.getReference(k);
	let gottenMimetype = a.getMimetype(k);
	let expectedRef = c.hashes[collection_idx];
	let expectedMimetype = idx.intermediate[value_idx];
	for (let i = 0; i < 32; i++) {
		if (expectedRef[i] != gottenRef[i]) {
			console.error('manifest ref mismatch', k);
			process.exit(1);
		}
		if (expectedMimetype[i] != gottenMimetype[i]) {
			console.error('manifest miemtype mismatch', k, expectedMimetype, gottenMimetype);
			process.exit(1);
		}
	}
}
console.debug('ls', a.list(''));
//console.debug('ls', a.list('/'));
console.debug('ls', a.list('media/'));
//console.debug('ls', a.list('/media/'));
//console.debug('ls', a.list('/media'));
//console.debug('ls', a.list('/media//'));


// create the collection that will wrap the two chunk vectors
let w = new collection.Collection();

let refCollection = hasher.Hash(c_serialized);
w.add(refCollection);

let refIndex = hasher.Hash(idx_serialized);
w.add(refIndex);

let w_serialized = w.serialize();
console.debug('wrapper serialized', w_serialized);

hasher.Reset();
let root = hasher.Hash(w_serialized);

console.debug('endpoint root hash', root);
