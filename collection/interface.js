function CollectionIndex(name) {
	this.name = name;
	this.keys = {}; // keys to value index
	this.values = [];
}

CollectionIndex.prototype.add = function(key, value) {
	return true;
}

// returns Uint8Array
CollectionIndex.prototype.serialize = function() { 
	return new Uint8Array();
}

// takes Uint8Array
CollectionIndex.prototype.deserialize = function(b) {
	return true;
}

CollectionIndex.prototype.toString = function() {
	return "";
}


function Collection() {
	this.hashes = []; // Uint8Arrays of hashes
	this.indices = {}; // name -> CollectionIndex
}

Collection.prototype.add = function(hash) {
	return true;
}

Collection.prototype.addIndex = function(idx) {
	return 0; // index of ... eh ... index
}

// executes f for every element in collection with element's reference as argument
Collection.prototype.each = function(f, mode) { // modes: order of insert, lexiographical order
	return 0; // amount of elements
}

// returns Uint8Array
Collection.prototype.serialize = function() {
	return new Uint8Array();
}

// takes Uint8Array
Collection.prototype.deserialize = function(b) {
	return true;
}

Collection.prototype.toString = function() {
	return "";
}

// SYNOPSIS

let refs = [
	new Uint8Array(32),
	new Uint8Array(32),
	new Uint8Array(32),
];

let collection = new Collection();

// add the hashes, order will be preserved
collection.add(ref[0]); // 0xab ..
collection.add(ref[1]); // 0x12 ..
collection.add(ref[2]); // 0xbd ..

// will output debug hash ids 0 and 1
collection.each(console.debug);

// create a new index and add it to the collection
let idx = new CollectionIndex("manifest")
collection.addIndex(idx);

// add the metadata with references to object postitions
metadataKey = {
	filename: "foo.txt",
	mimetype: "text/plain",
}
idx.add(metadataKey, 0);

metadataKey = {
	filename: "media/bar.png",
	mimetype: "image/png",
}
idx.add(metadataKey, 1);

// content can have multiple pointers
metadataKey = {
	filename: "meditation/baz.txt",
	mimetype: "text/plain",
}
idx.add(metadataKey, 0);

// content can have multiple pointers
metadataKey = {
	filename: "xyxxy",
	mimetype: "application/octet-stream",
}
idx.add(metadataKey, 2);

// serialize in form that will be ingested into bee
let indexBytes = idx.serialize();
console.debug(indexBytes);
// example serialization format
// \x12foo.txt:text/plain\x00\x04medi\x13a/bar.png:image/png\x01\x19tation/baz.txt:text/plain\x00\x00\x1dxyxxy:application/octet-stream
// ^ first index character length
//            ^ separates filename and mime
//                       ^ index in content array
//                               ^ no mime means fork
//                                   ^ no index in content array here since it is not leaf
//                                                      ^ still forking, prefix of last fork will be prepended
//                                                          ^ another content array index
//                                                                                               ^ exit fork
//                                                                                                   ^ length of last element
//                                                                                                                                     ^ EOF
//                                                                                   
// for a hackathon we might be too lazy for that though, so  serialization can also be:
// {
// "foo.txt": [
//	0, "text/plain",
// ],
// "medi": {
//	"a/bar.png": [
//		1, "image/png",
// 	],
// 	"tation/bar.txt":[
//		0, "text/plain",
// 	],
// },
// "xyzzy": {
//	2, "application/octet-stream",
// }

let collectionBytes = collection.serialize();
let bzz = new SwarmAdapter();
let collectionRef  = bzz.Put(collectionBytes);
let manifestRef = bzz.Put(indexBytes);
// ohh, wrappable
let outerCollection = new Collection();
outerCollection.add(collectionRef);
outerCollection.add(manifestRef);
outerCollectionBytes = outerCollection.serialize();
let rootRef = bzz.Put(outerCollectionBytes);


// now lets get back out from swarm
let outerCollectionBytes = bzz.Get(rootRef);
let outerCollection = new Collection().deserialize(collectionBytes);
let collectionRefs = [];
outerCollection.each((el) => {
	collectionRefs.push(el);
});
let collectionBytes = bzz.Get(collectionRefs[0]);
let manifestBytes = bzz.Get(collectionRefs[1]);

let manifest = new CollectionIndex(manifestBytes).serialize(manifestBytes);
let collection = new Collection(collectionBytes).serialize(collectionBytes);
// ta-da
collection.addIndex(manifest);



// before hashes were ordered by insert order. this is good for eg. append stream.
// but what if we want static data snapshot that has canonical proof?
// indices can be built on canonical ordering too:
// get the list 
let canonical = [];
collection.each((el, "unordered") => {
	canonical.push(el);
});
// canonical will now be ref idxs 1,0,2
// [0x12, 0xab, 0xbd]

let unorderedIdx = new CollectionIndex("unorderedmanifest");
collection.addIndex(idx);

// now the indices of course will be different
metadataKey = {
	filename: "foo.txt",
	mimetype: "text/plain",
}
idx.add(metadataKey, 1);

metadataKey = {
	filename: "media/bar.png",
	mimetype: "image/png",
}
idx.add(metadataKey, 0);

// content can have multiple pointers
metadataKey = {
	filename: "meditation/baz.txt",
	mimetype: "text/plain",
}
idx.add(metadataKey, 1);

// content can have multiple pointers
metadataKey = {
	filename: "xyxxy",
	mimetype: "application/octet-stream",
}
idx.add(metadataKey, 2);

// and lazy output:
// {
// "foo.txt": [
//	1, "text/plain",
// ],
// "medi": {
//	"a/bar.png": [
//		0, "image/png",
// 	],
// 	"tation/bar.txt":[
//		1, "text/plain",
// 	],
// },
// "xyzzy": {
//	2, "application/octet-stream",
// }


